package appdev.magik.sced3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LicenceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_licence);
        setTitle(R.string.licence_activity_title);
    }
}
