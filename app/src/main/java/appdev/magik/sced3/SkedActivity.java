package appdev.magik.sced3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class SkedActivity extends AppCompatActivity {

    SharedPreferences preferences;
    Calendar cal;
    String setting_1_ext;
    String setting_2_ext;
    String title_from_site = "";
    boolean isTimeToReload = false;

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private ArrayList<Sked> Groupe_List;
    String[] daysOfWeekArray;
    String[] timetableArray;
    String[] numberSet;
    boolean isErrorWhileConnect = false;

    //private RecyclerView.Adapter adapter;
    private SkedAdapter skedAdapter;
    SkedActivity.Content content;

    int dayOfWeekPosition = 0;                          //Порядковый номер для выделения цветом
    int dayWithTimetablePosition = -1;                   //Порядковый номер элемента с расписанием
    String dayWithTimetableDay;
    String dayWithTimetableLesson;
    String dayWithTimetableColor;
    Document doc;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerview);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        cal = Calendar.getInstance();

        mList = findViewById(R.id.main_list);

        Groupe_List = new ArrayList<>();
        skedAdapter = new SkedAdapter(getApplicationContext(), Groupe_List);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);  //???
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(skedAdapter);

        daysOfWeekArray = new String[]{getString(R.string.text_week_day_1), getString(R.string.text_week_day_2), getString(R.string.text_week_day_3),
                getString(R.string.text_week_day_4), getString(R.string.text_week_day_5), getString(R.string.text_week_day_6), getString(R.string.text_week_day_7)};

        //timetableArray = new String[]{"08:00-09:30", "09:40-11:10", "11:30-13:00", "13:10-14:40", "14:50-16:20", "16:30-18:00", "18:10-19:40", "19:50-21:20"};
        timetableArray = new String[]{"", "", "", "", "", "", "", ""};

        //numberSet = new String[]{"❶", "❷", "❸", "❹", "❺", "❻", "❼", "❽", "❾", "❿"};
        numberSet = new String[]{"➀", "➁", "➂", "➃", "➄", "➅", "➆", "➇", "➈", "➉"};

        setTitle(getString(R.string.text_loading));

        checkUpdate();



    }

    private class Content extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            doc = Jsoup.parse(preferences.getString("extra_Setting_cache", " "));

            //progressBar.setVisibility(View.VISIBLE);
            //progressBar.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Document docConnect = Jsoup.connect(preferences.getString("extra_Setting_2", getString(R.string.url_schedule_default))).timeout(7000).get();

                if(!doc.text().equals(docConnect.text())){
                    Snackbar.make(findViewById(android.R.id.content), getString(R.string.text_new_schedule), Snackbar.LENGTH_LONG)
                            .show();
                    doc = docConnect;
                }

                //Log.d("CacheData", doc.text());
                //Log.d("NewData", docConnect.text());
                parseData(doc);

                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("extra_Setting_cache", String.valueOf(doc));
                //editor.commit();
                editor.apply();

            } catch (IOException e) {
                e.printStackTrace();
                parseData(doc);

                isErrorWhileConnect = true;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //progressBar.setVisibility(View.GONE);
            //progressBar.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_out));
            if (isErrorWhileConnect) {
                //Toast.makeText(getApplicationContext(), getString(R.string.text_warning), Toast.LENGTH_SHORT).show();
                Snackbar.make(findViewById(android.R.id.content), getString(R.string.text_warning), Snackbar.LENGTH_LONG)
                        .show();
                isErrorWhileConnect = false;
            }



            skedAdapter.notifyDataSetChanged();
            try{
                mList.scrollToPosition(dayOfWeekPosition);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //mList.scrollToPosition(dayOfWeekPosition);

            //Log.d("title_from_site",title_from_site);
            //Log.d("preferences",preferences.getString("extra_Setting_1", getString(R.string.group_name)));

            title_from_site = title_from_site.replace(" Неделя: 1-я", "");
            title_from_site = title_from_site.replace(" Неделя: 2-я", "");

            //if (title_from_site.contains(preferences.getString("extra_Setting_1", getString(R.string.group_name))) || preferences.getBoolean("Setting_isTeacher", false)){
            if (preferences.getString("extra_Setting_1", getString(R.string.group_name)).contains(title_from_site)) {
                setTitleThis(title_from_site);
            } else {
                //setTitle(getString(R.string.url_need_reset));
                Groupe_List.clear();
                startActivity(new Intent(SkedActivity.this, StartActivity.class));
                overridePendingTransition(0, 0);
                //SkedActivity.this.startActivity(new Intent(activity, StartActivity.class));
                //isNeedReset = true;
            }

            skedAdapter.setOnSkedClickListener(onSkedClickListener);

        }
    }

    void parseData(Document doc_) {
        title_from_site = doc_.select("font[face]").eq(2).text();
        Elements data = doc_.select("p[align]");

        /*
        String imgUrl = data.select("span.thumbnail")
                .select("img")
                .eq(i)
                .attr("src");

        String title = data.select("h4.gridminfotitle")
                .select("span")
                .eq(i)
                .text();

        String detailUrl = data.select("h4.gridminfotitle")
                .select("a")
                .eq(i)
                .attr("href");

         */

        //Получение расписания звонков:
        for (int v = 0; v < 8; v++) {
            timetableArray[v] = data.select("p[align]").eq(v + 11).text();
        }

        //Получение расписания пар:
        Sked sked;
        int startValue = 20;    //Начальное значение для получения
        int updValue = 0;       //Фильтрация пробелов между днями
        String dayLesson;
        String viewToHide = "00000000";  // 0 - view скрыт
        boolean isNotLastLesson = true;
        int hidenDays = 0;
        int systemDayOfWeek;

        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert manager != null;
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        width *= 0.042;

        for (int counterWeek = 0; counterWeek < 2; counterWeek++) {
            for (int counterDay = 0; counterDay < 7; counterDay++) {
                final StringBuilder stringBuilder = new StringBuilder();
                for (int counterLesson = 8; counterLesson >= 1; counterLesson--) {

                    int index = updValue + startValue + (counterDay * 8) + counterLesson + (counterWeek * 6 * 8);
                    if (isNotLastLesson && (data.select("p[align]").eq(index).text().isEmpty() || data.select("p[align]").eq(index).text().equals("_"))) {
                        continue;
                    } else {
                        isNotLastLesson = false;
                    }

                    //Костыль для отображения порядкового номера
                    if (preferences.getBoolean("Setting_symbol", true)) {
                        dayLesson = numberSet[counterLesson - 1] + " " + data.select("p[align]").eq(index).text() + "\n";
                    } else {
                        dayLesson = counterLesson + ". " + data.select("p[align]").eq(index).text() + "\n";
                    }

                    stringBuilder.insert(0, dayLesson);

                    if (dayLesson.length() > width && !preferences.getBoolean("Setting_symbol", true)) {
                        stringBuilder.insert(dayLesson.lastIndexOf(" ", width), "\n    ");
                    }
                }
                isNotLastLesson = true;
                updValue += 2;

                if (counterDay == 0 && counterWeek == 0) {
                    sked = new Sked((getString(R.string.text_week_1) + "\n" + daysOfWeekArray[counterDay]), stringBuilder.toString());
                } else if (counterDay == 0 && counterWeek == 1) {
                    sked = new Sked((getString(R.string.text_week_2) + "\n" + daysOfWeekArray[counterDay]), stringBuilder.toString());
                } else {
                    sked = new Sked(daysOfWeekArray[counterDay], stringBuilder.toString());
                }

                if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                    systemDayOfWeek = 7;
                } else {
                    systemDayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;
                }

                //Выделение текущего дня
                if ((cal.get(Calendar.WEEK_OF_YEAR) % 2 == 0) == (preferences.getBoolean("is_first_week", true)) == (counterWeek == 0) &&
                        ((counterDay + 1) == systemDayOfWeek)) {
                    dayOfWeekPosition = counterDay + (counterWeek * 7) - hidenDays;

                    //TODO цвет
                    sked.setColor("#8Db6b6e0");
                    Groupe_List.add(sked);
                } else if (stringBuilder.length() == 0 && (counterDay + 1 == 6 || counterDay + 1 == 7)) {
                    hidenDays++;
                } else {
                    Groupe_List.add(sked);
                }
            }
            updValue = 2;
            startValue *= 3;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        /*
        Groupe_List.clear();
        content = new SkedActivity.Content();
        content.execute();

        if (preferences.getString("extra_Setting_1", getString(R.string.group_name)).equals(getString(R.string.group_name))) {
            Intent intent = new Intent(this, StartActivity.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
        skedAdapter.setOnSkedClickListener(onSkedClickListener);

         */
    }

    @Override
    protected void onResume() {
        super.onResume();

        mList.getRecycledViewPool().clear();
        skedAdapter.notifyDataSetChanged();

        cal = Calendar.getInstance();
        Groupe_List.clear();
        content = new SkedActivity.Content();
        content.execute();

        if (preferences.getString("extra_Setting_1", getString(R.string.group_name)).equals(getString(R.string.group_name))) {
            Intent intent = new Intent(this, StartActivity.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
        }

        /*
        if(isTimeToReload){
            mWebView.loadUrl(setting_2_ext);
            //Log.e("WebViewActivity","Обновление");
            isTimeToReload = false;
        }
        */
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    @Override
    protected void onStop() {
        skedAdapter.setOnSkedClickListener(null);
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //reloadTimer();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(SkedActivity.this, SettingsActivity.class));
                overridePendingTransition(0, 0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
    public void reloadTimer() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Log.e("WebViewActivity","Таймер");
                reloadTimer();
                isTimeToReload = true;
            }
        }, 60000);
    }

     */

    void setTitleThis(String title) {
        if ((cal.get(Calendar.WEEK_OF_YEAR) % 2 == 0) == (preferences.getBoolean("is_first_week", true))) {
            setTitle(title + " | " + getString(R.string.text_week_1));
        } else {
            setTitle(title + " | " + getString(R.string.text_week_2));
        }
    }

    public void checkUpdate() {

        if (preferences.getBoolean("Setting_update", true)) {
            String setting_3_ext = preferences.getString("extra_Setting_3", getString(R.string.url_api));
            JsonObjectRequest jsonRequest = new JsonObjectRequest
                    (Request.Method.GET, setting_3_ext, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (!(BuildConfig.VERSION_NAME.equals(response.getString("version")))) {
                                    dialogUpdate();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });

            Volley.newRequestQueue(this).add(jsonRequest);
        }
    }

    public void dialogUpdate() {
        LayoutInflater inflater = LayoutInflater.from(SkedActivity.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final String setting_4_ext = preferences.getString("extra_Setting_4", getString(R.string.url_download));
        builder.setTitle(getString(R.string.text_new_v_ready));
        builder.setMessage(getString(R.string.text_new_v_ready_all));

        builder.setCancelable(false);
        AlertDialog alertDialog = builder.create();

        builder.setPositiveButton(getString(R.string.text_update), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //SharedPreferences.Editor editor = preferences.edit();
                //editor.clear();
                //editor.commit();

                Uri address = Uri.parse(setting_4_ext);
                Intent openlinkIntent = new Intent(Intent.ACTION_VIEW, address);
                startActivity(openlinkIntent);
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.text_not_now), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    //Действия при нажатии
    private final SkedAdapter.OnSkedClickListener onSkedClickListener = new SkedAdapter.OnSkedClickListener() {
        @Override
        public void onSkedClick(String skedDay, String skedLesson, String skedColor, int pos) {
            skedAdapter.setOnSkedClickListener(null);
            String skedLessonModify = skedLesson;

            for (int i = 1; i <= 8; i++) {
                if (preferences.getBoolean("Setting_symbol", true)) {
                    skedLessonModify = skedLessonModify.replace(numberSet[i - 1] + " ", (numberSet[i - 1] + " " + timetableArray[i - 1] + "\n"));
                } else {
                    skedLessonModify = skedLessonModify.replace(i + ". ", (i + ". " + timetableArray[i - 1] + "\n     "));
                }
            }

            if (dayWithTimetablePosition == -1 && !skedLesson.isEmpty()) {      //Первоначальное нажатие на item
                //Задание нового
                dayWithTimetableDay = skedDay;
                dayWithTimetablePosition = pos;
                dayWithTimetableColor = skedColor;
                dayWithTimetableLesson = skedLesson;
                Groupe_List.set(pos, new Sked(skedDay, skedLessonModify, skedColor));
                skedAdapter.notifyItemChanged(pos);
            } else if (dayWithTimetablePosition == pos) {                       //Повторное нажатие на item(Сворачивание)
                //Возвращение предыдущего
                dayWithTimetablePosition = -1;
                Groupe_List.set(pos, new Sked(dayWithTimetableDay, dayWithTimetableLesson, dayWithTimetableColor));
                skedAdapter.notifyItemChanged(pos);
            } else if (!skedLesson.isEmpty()) {                                 //Раскрытие нового item, сворачивание предыдущего
                //Возвращение предыдущего
                Groupe_List.set(dayWithTimetablePosition, new Sked(dayWithTimetableDay, dayWithTimetableLesson, dayWithTimetableColor));
                skedAdapter.notifyItemChanged(dayWithTimetablePosition);
                //Задание нового
                dayWithTimetableDay = skedDay;
                dayWithTimetablePosition = pos;
                dayWithTimetableColor = skedColor;
                dayWithTimetableLesson = skedLesson;
                Groupe_List.set(pos, new Sked(skedDay, skedLessonModify, skedColor));
                skedAdapter.notifyItemChanged(pos);
            }

            skedAdapter.setOnSkedClickListener(onSkedClickListener);

        }
    };
}