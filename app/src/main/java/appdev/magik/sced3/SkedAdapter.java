package appdev.magik.sced3;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class SkedAdapter extends RecyclerView.Adapter<SkedAdapter.ViewHolder> {

    private Context context;
    private List<Sked> list;

    @Nullable
    public OnSkedClickListener onSkedClickListener;

    public void setOnSkedClickListener(@Nullable OnSkedClickListener onSkedClickListener) {
        this.onSkedClickListener = onSkedClickListener;
    }

    public SkedAdapter(Context context, List<Sked> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.recycle_item_sked, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Sked el = list.get(position);

        holder.textDayOfWeek.setText(el.getDayOfWeek());
        holder.textLesson.setText(el.getLesson());
        holder.itemView.setBackgroundColor(Color.parseColor(el.getColor()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textDayOfWeek, textLesson;

        public ViewHolder(View itemView) {
            super(itemView);
            textDayOfWeek = itemView.findViewById(R.id.text_dayOfWeek);
            textLesson = itemView.findViewById(R.id.text_sked);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        int position = getAdapterPosition();
                        if (onSkedClickListener != null && list.size() != 0) {
                            Sked sked = list.get(position);
                            onSkedClickListener.onSkedClick(sked.getDayOfWeek(), sked.getLesson(), sked.getColor(), position);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
            });
        }
    }

    public interface OnSkedClickListener {
        void onSkedClick(String skedDay, String skedLesson, String color, int position);
    }

}