package appdev.magik.sced3;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private Context context;
    private List<Data> list;

    @Nullable
    public OnDataClickListener onDataClickListener;

    public void setOnDataClickListener(@Nullable OnDataClickListener onDataClickListener) {
        this.onDataClickListener = onDataClickListener;
    }

    public DataAdapter(Context context, List<Data> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.recycle_item_groupe, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Data data = list.get(position);

        holder.textTitle.setText(data.getTitle());
        holder.textTitle.setTag(data.getURL());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_groupe);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Data data = list.get(position);

                    if (onDataClickListener != null) {
                        onDataClickListener.onDataClick(data.getTitle(), data.getURL());
                    }
                }
            });
        }
    }

    public interface OnDataClickListener {
        void onDataClick(String dataTitle, String dataURL);
    }

}
