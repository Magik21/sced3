package appdev.magik.sced3;

public class Sked {

    public String dayOfWeek;
    public String lesson;
    public String color = "#00FFFFFF";
    public String viewToHide = "00000000";      //0 - hide, 1 - view


    public Sked() {
    }

    public Sked(String dayOfWeek, String lesson) {
        this.dayOfWeek = dayOfWeek;
        this.lesson = lesson;
    }

    public Sked(String dayOfWeek, String lesson, String color) {
        this.dayOfWeek = dayOfWeek;
        this.lesson = lesson;
        this.color = color;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public String getLesson() {
        return lesson;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getViewToHide() {
        return viewToHide;
    }

    public void setViewToHide(String viewToHide) {
        this.viewToHide = viewToHide;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

}