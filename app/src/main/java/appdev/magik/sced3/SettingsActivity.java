package appdev.magik.sced3;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        Preference docPref = (Preference) findPreference("sked");
        docPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(SettingsActivity.this, StartActivity.class));
                overridePendingTransition(0, 0);
                return true;
            }
        });

        Preference qr = (Preference) findPreference("qrurl");
        qr.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(SettingsActivity.this, QRActivity.class));
                overridePendingTransition(0, 0);
                return true;
            }
        });

        Preference licence = (Preference) findPreference("licence");
        licence.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(SettingsActivity.this, LicenceActivity.class));
                overridePendingTransition(0, 0);
                return true;
            }
        });

        Preference versPref = (Preference) findPreference("version");
        versPref.setTitle(getString(R.string.text_version) + ": " + BuildConfig.VERSION_NAME);
        versPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Uri address = Uri.parse(getString(R.string.url_site));
                Intent openlinkIntent = new Intent(Intent.ACTION_VIEW, address);
                startActivity(openlinkIntent);
                return true;
            }
        });
    }
}