package appdev.magik.sced3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class StartActivity extends AppCompatActivity {

    SharedPreferences preferences;
    String setting_1_ext;
    //String group_list = "";
    //String URL_list = "";

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private ArrayList<Data> Groupe_List = new ArrayList<>();
    //private List<DayOfWeek> DayOfWeek_List;

    //private RecyclerView.Adapter adapter;
    private DataAdapter dataAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerview);

        setTitle(R.string.start_activity_title);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        mList = findViewById(R.id.main_list);
        Groupe_List = new ArrayList<>();
        dataAdapter = new DataAdapter(getApplicationContext(), Groupe_List);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(dataAdapter);

        listCreate();
    }

    void listCreate(){

        Groupe_List.add(new Data("1 Курс", "1"));
        Groupe_List.add(new Data("2 Курс", "2"));
        Groupe_List.add(new Data("3 Курс", "3"));
        Groupe_List.add(new Data("4 Курс", "4"));
        Groupe_List.add(new Data("5 Курс", "5"));
        Groupe_List.add(new Data("Преподаватель", "teachers"));
        Groupe_List.add(new Data("Прочее", "other"));
        dataAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    @Override
    protected void onStart() {
        super.onStart();
        dataAdapter.setOnDataClickListener(onDataClickListener);
    }

    @Override
    protected void onStop() {
        dataAdapter.setOnDataClickListener(null);
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(StartActivity.this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private final DataAdapter.OnDataClickListener onDataClickListener = new DataAdapter.OnDataClickListener() {
        @Override
        public void onDataClick(String dataTitle, String dataURL) {
            //Действия при нажатии
            Intent intent = new Intent(getBaseContext(), StartActivitySecond.class);
            intent.putExtra("tag", dataURL);
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
    };

}