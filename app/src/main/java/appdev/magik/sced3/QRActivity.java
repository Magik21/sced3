package appdev.magik.sced3;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.WriterException;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class QRActivity extends AppCompatActivity {

    ImageView qrImage;
    QRGEncoder qrgEncoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.qr_activity_title);
        setContentView(R.layout.activity_qr);
        qrImage = (ImageView) findViewById(R.id.QR_imageView);
    }

    @Override
    protected void onStart() {
        super.onStart();

        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);

        int width = point.x;
        int height = point.y;

        Log.e("width: ", String.valueOf(width));

        int smallerDimension = Math.min(width, height);
        smallerDimension = smallerDimension * 3 / 4;
        qrgEncoder = new QRGEncoder(
                getString(R.string.url_site), null,
                QRGContents.Type.TEXT,
                smallerDimension);
        try {
            qrImage.setImageBitmap(qrgEncoder.encodeAsBitmap());
        } catch (WriterException e) {
            //Log.v(TAG, e.toString());
        }
    }
}
