package appdev.magik.sced3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class StartActivitySecond extends AppCompatActivity {

    SharedPreferences preferences;
    String setting_1_ext;
    //String group_list = "";
    //String URL_list = "";
    String tag = "";

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private ArrayList<Data> GroupeList = new ArrayList<>();
    //private List<DayOfWeek> DayOfWeek_List;

    //private RecyclerView.Adapter adapter;
    private DataAdapter dataAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerview);

        setTitle( getString(R.string.text_loading));

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        mList = findViewById(R.id.main_list);
        GroupeList = new ArrayList<>();
        dataAdapter = new DataAdapter(getApplicationContext(), GroupeList);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(dataAdapter);

        /*
        RecyclerView recyclerView = findViewById(R.id.main_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addItemDecoration(new DividerItemDecoration(this, layoutManager.getOrientation()));

        dataAdapter = new DataAdapter();
        recyclerView.setAdapter(dataAdapter);

        //===========

        mWebView = (WebView) findViewById(R.id.webView);
        CookieManager cookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.setAcceptThirdPartyCookies(mWebView, true);
        } else {
            cookieManager.setAcceptCookie(true);
        }

        setting_1_ext = preferences.getString("extra_Setting_1", getString(R.string.url_start));
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.loadUrl(setting_1_ext);
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);

         */

        Intent intent = getIntent();
        tag = intent.getStringExtra("tag");

        Content content = new Content();
        content.execute();
    }

    private class Content extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
            //progressBar.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //progressBar.setVisibility(View.GONE);
            //progressBar.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_out));
            /*
            for (int i = 0; i < group_list.split(";").length; ++i){
                String Group = group_list.split(";")[i];
                String URL = URL_list.split(";")[i];
                DayOfWeek oneDay_ass = new DayOfWeek();
                oneDay_ass.setTitle(Group);
                oneDay_ass.setURL(URL);
                DayOfWeek_List.add(oneDay_ass);
            }
             */
            setTitle(R.string.start_activity_title);
            dataAdapter.notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Document doc;
            Elements data;

            if(tag.equals("teachers")){
                String[] urlTeachers = getResources().getStringArray(R.array.urlTeachers);
                for(String urlTeacher : urlTeachers) {
                    try {
                        doc = Jsoup.connect(urlTeacher).get();
                        data = doc.select("a[href]");

                        for (Element link : data) {
                            String group_list = link.text();
                            if (group_list.isEmpty()) {
                                continue;
                            }

                            GroupeList.add(new Data(group_list, link.absUrl("href")));

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                String[] urlSchedules = getResources().getStringArray(R.array.urlSchedules);
                for(String urlSchedule : urlSchedules) {
                    try {
                        doc = Jsoup.connect(urlSchedule).get();
                        data = doc.select("a[href]");

                        for (Element link : data) {
                            String group_list = link.text();
                            if (group_list.isEmpty()) {
                                continue;
                            }

                            if (group_list.contains("-" + tag)) {
                                GroupeList.add(new Data(group_list, link.absUrl("href")));
                            } else if (tag.equals("other") && !group_list.contains("-1") &&
                                    !group_list.contains("-2") && !group_list.contains("-3") &&
                                    !group_list.contains("-4") && !group_list.contains("-5")) {
                                GroupeList.add(new Data(group_list, link.absUrl("href")));
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
            return null;
        }
    }

    /*
    @Override
    public void onBackPressed() {
        finishAffinity();
    }

     */

    @Override
    protected void onStart() {
        super.onStart();
        dataAdapter.setOnDataClickListener(onDataClickListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        tag = intent.getStringExtra("tag");
    }

    @Override
    protected void onStop() {
        dataAdapter.setOnDataClickListener(null);
        super.onStop();
    }



    /*
        @Override
        protected void onResume() {
            super.onResume();

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Синхронизация...");
            progressDialog.show();
            DayOfWeek_List.clear();
            //getHtmlFromWeb();
            Log.e("Test", "Как бы кек");

            for (int i = 0; i < group_list.split(";").length; ++i){
                String Group = group_list.split(";")[i];
                String URL = URL_list.split(";")[i];
                DayOfWeek oneDay_ass = new DayOfWeek();
                oneDay_ass.setTitle(Group);
                oneDay_ass.setURL(URL);
                DayOfWeek_List.add(oneDay_ass);
            }

            adapter.notifyDataSetChanged();
            progressDialog.dismiss();
            //setting_1_ext = preferences.getString("extra_Setting_1", getString(R.string.url_start));


        }
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(StartActivitySecond.this, SettingsActivity.class));
                //dialogConfirm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void dialogConfirm(final String dataTitle, final String dataURL) {
        LayoutInflater inflater = LayoutInflater.from(StartActivitySecond.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.text_save_groupe)); // Сохранить ссылку?
        builder.setMessage(dataTitle);

        builder.setCancelable(false);
        AlertDialog alertDialog = builder.create();

        builder.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("extra_Setting_1", dataTitle);
                editor.putString("extra_Setting_2", dataURL);

                dialogWeek();

                editor.commit();
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void dialogWeek() {
        LayoutInflater inflater = LayoutInflater.from(StartActivitySecond.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.text_now_week));
        builder.setMessage(getString(R.string.text_ontap));

        builder.setCancelable(true);
        AlertDialog alertDialog = builder.create();

        builder.setNegativeButton(getString(R.string.text_week_1), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Calendar cal = Calendar.getInstance();
                SharedPreferences.Editor editor = preferences.edit();

                if(cal.get(Calendar.WEEK_OF_YEAR) % 2 == 0) {
                    editor.putBoolean("is_first_week", true);
                } else {
                    editor.putBoolean("is_first_week", false);
                }

                editor.commit();
                startActivity(new Intent(getBaseContext(), SkedActivity.class));
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.text_week_2), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Calendar cal_ = Calendar.getInstance();
                SharedPreferences.Editor editor = preferences.edit();

                if(cal_.get(Calendar.WEEK_OF_YEAR) % 2 != 0) {
                    editor.putBoolean("is_first_week", true);
                } else {
                    editor.putBoolean("is_first_week", false);
                }

                editor.commit();
                startActivity(new Intent(getBaseContext(), SkedActivity.class));
                dialog.cancel();
            }
        });
        builder.show();
    }

    private final DataAdapter.OnDataClickListener onDataClickListener = new DataAdapter.OnDataClickListener() {
        @Override
        public void onDataClick(String dataTitle, String dataURL) {
            //Действия при нажатии
            dialogConfirm(dataTitle, dataURL);
        }
    };

}